//
//  ViewController.h
//  Protocols
//
//  Created by Nima Afazel on 29/10/15.
//  Copyright (c) 2015 Creationbox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// Forward declaration de la clase de MiVista. Hacemos esto porque si importamos y en la clase de MiVista importamos esta clase se hace un loop.
@class MiVista;

// 1.- Forward declaration del protocolo para el compilador.
@protocol MiProtocoloDelegate;

@interface ViewController : NSViewController

// 2.- Creamos un a propiedad que se llame "delegate" (por standard) del tipo id (que cualquier clase la pueda implementar) que acceda a nuestro protocolo.
@property (nonatomic, assign) id<MiProtocoloDelegate> delegate;

@property (weak) IBOutlet MiVista *miVista;  // propiedad IBOutlet de la vista

- (IBAction)clickBoton:(id)sender;
@end


// 3.- Creamos nuestro protocolo. Si no le ponemos nada a nuestros métodos, serán obligatorias.
// Si le ponemos @optional... Pues se entiende, no?
@protocol MiProtocoloDelegate

// 4.- Método que manda un color para que lo cambien en donde lo cachen.
-(void)cambiaColor:(CGColorRef)miColor;

@end
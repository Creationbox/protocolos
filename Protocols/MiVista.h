//
//  MiVista.h
//  Protocols
//
//  Created by Nima Afazel on 29/10/15.
//  Copyright (c) 2015 Creationbox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"  // 6.- Importamos nuestra clase para que reconozca el protocolo

@interface MiVista : NSView <MiProtocoloDelegate>  // 7.- Le decimos a nuestra view que implemente el protocolo

@end

//
//  MiVista.m
//  Protocols
//
//  Created by Nima Afazel on 29/10/15.
//  Copyright (c) 2015 Creationbox. All rights reserved.
//

#import "MiVista.h"

@implementation MiVista

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

-(void)awakeFromNib
{
    // al inicio le damos un fondo rojo
    [self setWantsLayer:YES];
    [self.layer setBackgroundColor:[[NSColor redColor] CGColor]];
}

// 8.- Implementamos los metodos forzosos del protocolo
-(void)cambiaColor:(CGColorRef)miColor
{
    // Y hacemos lo que queramos.
    // En este caso tomo el color y se lo cambio al fondo de mi view.
    [self.layer setBackgroundColor:miColor];
}

@end

//
//  AppDelegate.h
//  Protocols
//
//  Created by Nima Afazel on 29/10/15.
//  Copyright (c) 2015 Creationbox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end


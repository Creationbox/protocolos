//
//  ViewController.m
//  Protocols
//
//  Created by Nima Afazel on 29/10/15.
//  Copyright (c) 2015 Creationbox. All rights reserved.
//

#import "ViewController.h"


@implementation ViewController

// 5.- Sintetizamos la propiedad.
@synthesize delegate, miVista;

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
    // 10.- Le decimos a nuestro ViewController quien es el que va a implementar su protocolo.
    // En este caso es nuestra vista.
    // Le hacemos un cast para que no salga un warning.
    [self setDelegate:(id<MiProtocoloDelegate>)miVista];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)clickBoton:(id)sender {
    // 9.- al darle click al boton le mandamos el mensaje por medio de nuestro delegate
    [self.delegate cambiaColor:[[NSColor blueColor] CGColor]];
}
@end
